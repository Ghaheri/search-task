import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _16266795 = () => interopDefault(import('../pages/dashboard.vue' /* webpackChunkName: "pages/dashboard" */))
const _156d966a = () => interopDefault(import('../pages/login.vue' /* webpackChunkName: "pages/login" */))
const _4e915f12 = () => interopDefault(import('../pages/search.vue' /* webpackChunkName: "pages/search" */))
const _4bc5f353 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/dashboard",
    component: _16266795,
    name: "dashboard"
  }, {
    path: "/login",
    component: _156d966a,
    name: "login"
  }, {
    path: "/search",
    component: _4e915f12,
    name: "search"
  }, {
    path: "/",
    component: _4bc5f353,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
